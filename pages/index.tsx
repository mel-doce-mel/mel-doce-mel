import type { FC, ReactElement, PropsWithChildren } from 'react';
import Image from 'next/image';
import { NextSeo } from 'next-seo';
import {
  Link as MuiLink,
  LinkProps as MuiLinkProps,
  Stack,
  useMediaQuery,
  useTheme,
} from '@mui/material';
import logo from 'images/logo.svg';

const Link: FC<MuiLinkProps> = (props) => (
  <MuiLink
    {...props}
    sx={{
      p: (theme) => theme.spacing(1, 0),
      fontFamily: 'Montserrat',
      fontSize: 24,
      color: 'secondary.main',
      textDecoration: 'none',
      textAlign: 'center',
      background: 'rgba(255,255,255,.5)',
      border: 'thin solid',
      borderColor: 'secondary.main',
      borderRadius: 3,
      boxShadow:
        '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
    }}
  />
);

const Index = () => {
  const theme = useTheme();
  const smallScreens = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <>
      <NextSeo
        title="Link"
        canonical="https://meldocemel.com.br/"
      />

      <Stack p={4} spacing={{ xs: 0, md: 4 }}>
        <Image
          src={logo}
          alt="Mel Doce Mel"
          width={smallScreens ? 300 : 800}
          height={smallScreens ? 300 : 304}
          sizes="100vw"
          style={{ margin: 'auto' }}
          priority
        />

        <Stack spacing={2}>
          {/*<Link target="_blank" href="/pascoa-2024.pdf">Páscoa</Link>*/}
          <Link href="/catalogo">Catálogo</Link>
          <Link target="_blank" href="/v2-Natal-2024.pdf">Natal</Link>
          <Link target="_blank" href="/bolos.pdf">Bolos caseiros</Link>
          <Link href="https://wa.me/5561996657197">WhatsApp</Link>
          <Link href="https://www.instagram.com/meldocemel.chocolateria/">Instagram</Link>
          <Link href="https://goo.gl/maps/Ub5Y41WroQzKDoBP6">Localização</Link>
        </Stack>
      </Stack>
    </>
  );
};

export default Index;
