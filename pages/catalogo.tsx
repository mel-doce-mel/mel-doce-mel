import { cloneElement } from 'react';
import type { FC, ReactElement, PropsWithChildren } from 'react';
import {
  Box,
  Link as MuiLink,
  Button,
  IconButton,
  Stack,
  AppBar as MuiAppBar,
  Toolbar,
  useMediaQuery,
  useTheme,
  useScrollTrigger,
} from '@mui/material';
import {
  HelpOutlineOutlined as HelpIcon,
  Favorite as HeartIcon,
  FmdGoodOutlined as PinIcon,
  Instagram as InstagramIcon,
  WhatsApp as WhatsAppIcon,
  ArrowBack as BackIcon,
} from '@mui/icons-material';
import Grid from '@mui/material/Unstable_Grid2';
import { NextSeo } from 'next-seo';
import { Masonry } from '@mui/lab';
import Image from 'next/image';
import {
  alfajores,
  barrasRecheadas,
  biscoitosAmanteigados,
  brownies,
  cestasDeCafeDaManha,
  cookies,
  embalagensParaPresente,
  kits,
  ornamentacoesPaesDeMel,
  paesDeMel,
  palhasItalianas,
  Link,
} from 'dados';
import {
  BotaoWhatsApp,
  Descricao,
  Footer,
  ImagensGrid,
  Legenda,
  ListaProdutos,
  Preco,
  Subtitulo,
  Titulo,
} from 'components';
import logo from 'images/logo.svg';
import paoDeMelImg from 'images/pao-de-mel.jpg';
import palhaItalianaImg from 'images/palha-italiana.jpg';
import alfajorImg from 'images/alfajor.jpg';
import cookieImg from 'images/cookie.jpg';
import barraFrutasVermelhasImg from 'images/barra-frutas-vermelhas.jpg';
import cestaCafeDaManhaImg from 'images/cesta-cafe-da-manha.jpg';
import barraCarameloSalgadoImg from 'images/barra-caramelo-salgado.jpg';
import caixaPaoDeMelImg from 'images/caixa-pao-de-mel.jpg';
import whatsAppImg from 'images/whatsapp.svg';

const SPACING = 6;

const HideOnScroll: FC<{ children: ReactElement }> = ({ children }) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
};

const AppBar: FC<PropsWithChildren> = ({ children }) => (
  <>
    <HideOnScroll>
      <MuiAppBar>
        <Toolbar>
          <Box display="flex" flex="1">
            {children}
          </Box>
        </Toolbar>
      </MuiAppBar>
    </HideOnScroll>
    <Toolbar />
  </>
);

const Index = () => {
  const theme = useTheme();
  const smallScreens = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <>
      <NextSeo
        title="Catálogo"
        canonical="https://meldocemel.com.br/catalogo"
      />

      <AppBar>
        <Stack direction="row" spacing={2}>
          <Button variant="outlined" color="secondary" onClick={() => (window.location.href = '/')}>
            <BackIcon color="secondary" fontSize="medium" />
          </Button>
          {/*
          <Button
            variant="outlined"
            color="secondary"
            onClick={() => window.open('/pascoa.pdf', '_blank')}
          >
            Páscoa
          </Button>
          */}
        </Stack>
        <Box flexGrow={1} />
        <Stack direction="row" spacing={2}>
          <Button
            variant="outlined"
            color="secondary"
            onClick={() => window.open(Link.GoogleMaps, '_blank')}
          >
            <PinIcon color="secondary" fontSize="medium" />
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            onClick={() => window.open(Link.Instagram, '_blank')}
          >
            <InstagramIcon color="secondary" fontSize="medium" />
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            onClick={() => window.open(Link.WhatsApp, '_blank')}
          >
            <WhatsAppIcon color="secondary" fontSize="medium" />
          </Button>
        </Stack>
      </AppBar>

      <Grid container>
        {/* Logo */}
        <Grid
          xs={12}
          lineHeight={0}
          display="flex"
          justifyContent="center"
          alignItems="center"
          bgcolor="tertiary.main"
          p={SPACING * 3}
        >
          <Image
            src={logo}
            alt="Mel Doce Mel"
            width={smallScreens ? 300 : 800}
            height={smallScreens ? 300 : 304}
            sizes="100vw"
          />
        </Grid>

        {/* Primeiro bloco */}
        <Grid xs={12} md={6} bgcolor="secondary.main" p={SPACING}>
          <Stack spacing={SPACING}>
            <ListaProdutos
              titulo="Pão de mel tradicional"
              itens={paesDeMel
                .filter((paoDeMel) => paoDeMel.tipo === 'Tradicional')
                .map(({ id, tamanho, diametro, valor }) => ({
                  id,
                  descricao: tamanho,
                  legenda: diametro.toString().concat('cm'),
                  valor,
                }))}
            />

            <ListaProdutos
              titulo={
                <span>
                  Adicionais{' '}
                  <MuiLink href="#adicionais-embalagens" color="text.primary" underline="always">
                    <HelpIcon fontSize="small" />
                  </MuiLink>
                </span>
              }
              itens={[
                ...embalagensParaPresente.map((embalagemParaPresente) => ({
                  ...embalagemParaPresente,
                  legenda: embalagemParaPresente.itens,
                })),

                ...ornamentacoesPaesDeMel.map((ornamentacaoPaesDeMel) => ({
                  ...ornamentacaoPaesDeMel,
                  legenda: ornamentacaoPaesDeMel.material,
                })),
              ].map(({ id, descricao, legenda, valor }) => ({
                id,
                descricao,
                legenda,
                valor,
              }))}
            />
          </Stack>
        </Grid>
        <Grid xs={12} md={6} lineHeight={0} position={{ xs: 'static', md: 'relative' }}>
          <Image
            src={paoDeMelImg}
            alt="Pães de mel"
            fill={!smallScreens}
            style={{
              maxWidth: '100%',
              height: smallScreens ? 'auto' : '100%',
              objectFit: 'cover',
              objectPosition: 'left bottom',
            }}
          />
        </Grid>

        {/* Segundo bloco */}
        <Grid xs={12} md={6} bgcolor="primary.main" p={SPACING}>
          <Stack spacing={SPACING}>
            <ListaProdutos
              titulo="Palhas italianas"
              itens={palhasItalianas.map(({ id, tamanho, lado, valor }) => ({
                id,
                descricao: tamanho,
                legenda: lado.toString().concat('cm'),
                valor,
              }))}
            />

            <ListaProdutos
              titulo="Brownies"
              itens={brownies.map(({ id, tamanho, diametro, valor }) => ({
                id,
                descricao: tamanho,
                legenda: diametro.toString().concat('cm'),
                valor,
              }))}
            />
          </Stack>
        </Grid>
        <Grid xs={12} md={6} bgcolor="primary.main" sx={{ p: SPACING, pt: { xs: 0, md: SPACING } }}>
          <Stack spacing={SPACING}>
            <ListaProdutos
              titulo="Alfajores"
              itens={alfajores.map(({ id, sabor, tamanho, diametro, valor }) => ({
                id,
                descricao: sabor,
                legenda: tamanho.concat(' • ', diametro.toString(), 'cm'),
                valor,
              }))}
            />

            <ListaProdutos
              titulo="Barras recheadas"
              itens={barrasRecheadas.map(({ id, sabor, comprimento, valor }) => ({
                id,
                descricao: sabor,
                legenda: comprimento.toString().concat('cm'),
                valor,
              }))}
            />
          </Stack>
        </Grid>

        {/* Terceiro bloco */}
        <Grid xs={12} md={6} lineHeight={0} position={{ xs: 'static', md: 'relative' }}>
          <Image
            src={palhaItalianaImg}
            alt="Palhas italianas"
            fill={!smallScreens}
            style={{
              maxWidth: '100%',
              height: smallScreens ? 'auto' : '100%',
              objectFit: 'cover',
              objectPosition: 'right bottom',
            }}
          />
        </Grid>
        <Grid xs={12} md={6} bgcolor="secondary.main" p={SPACING}>
          <Stack spacing={SPACING}>
            <ListaProdutos
              titulo="Biscoitos amanteigados"
              itens={biscoitosAmanteigados.map(({ id, sabor, peso, valor }) => ({
                id,
                descricao: sabor,
                legenda: peso.toString().concat('g'),
                valor,
              }))}
            />

            <ListaProdutos
              titulo="Cookies"
              itens={cookies.map(({ id, tipo, tamanho, diametro, valor }) => ({
                id,
                descricao: tipo,
                legenda: tamanho.concat(' • ', diametro.toString(), 'cm'),
                valor,
              }))}
            />
          </Stack>
        </Grid>

        {/* Quarto bloco */}
        <ImagensGrid
          imgs={[
            { src: alfajorImg, alt: 'Alfajores' },
            { src: cookieImg, alt: 'Cookies' },
            { src: barraFrutasVermelhasImg, alt: 'Barra de frutas vermelhas' },
          ]}
        />

        {/* Quinto bloco */}
        <Grid xs={12} md={6} bgcolor="primary.main" p={SPACING}>
          <Stack spacing={2}>
            <ListaProdutos
              titulo="Cestas de café da manhã"
              itens={cestasDeCafeDaManha.map(({ id, porcao, valor }) => ({
                id,
                descricao: porcao,
                valor,
              }))}
            />

            <Stack spacing={2} pt={2}>
              <Subtitulo>Componentes das cestas</Subtitulo>
              <Stack spacing={2}>
                {cestasDeCafeDaManha[0].itens.map((item) => (
                  <Legenda key={item}>{item};</Legenda>
                ))}
              </Stack>
            </Stack>
          </Stack>
        </Grid>
        <Grid xs={12} md={6} bgcolor="primary.main" sx={{ p: SPACING, pt: { xs: 0, md: SPACING } }}>
          <ListaProdutos
            titulo="Kits na caixinha"
            itens={kits.map(({ id, titulo, itens, valor }) => ({
              id,
              descricao: titulo,
              subitens: itens.map((item) => item.concat(';')),
              valor,
            }))}
          />
        </Grid>

        {/* Sexto blocos */}
        <ImagensGrid
          imgs={[
            { src: cestaCafeDaManhaImg, alt: 'Cesta de café da manhã' },
            { src: barraCarameloSalgadoImg, alt: 'Barra de caramelo salgado' },
            { src: caixaPaoDeMelImg, alt: 'Caixa de pães de mel' },
          ]}
        />

        {/* Sétimo bloco */}
        <Grid xs={12} bgcolor="secondary.main" p={SPACING}>
          <Stack spacing={2}>
            <Titulo>Perguntas frequentes</Titulo>

            <Box>
              <Masonry columns={{ xs: 1, md: 3 }} spacing={2}>
                <Stack spacing={2} pb={2}>
                  <Descricao>
                    <span id="adicionais-embalagens">Quais as embalagens disponíveis?</span>
                  </Descricao>
                  <Legenda>
                    Os valores dos produtos são referentes às embalagens padrão. Para ocasiões
                    especiais, o cliente disponibiliza a embalagem desejada ou arca com o custo
                    adicional.
                    <br />
                    <br />O custo de impressão da tag depende do prazo e quantidade (gráfica em SP,
                    melhor qualidade e custo-benefício).
                  </Legenda>
                </Stack>

                <Stack spacing={2} pb={2}>
                  <Descricao>Como eu conservo meu produto?</Descricao>
                  <Legenda>
                    Nossos pães de mel não precisam ir à geladeira, mantenham-os em{' '}
                    <strong>local fresco</strong>. Sua validade é <strong>de 20 à 30 dias</strong>.
                  </Legenda>
                </Stack>

                <Stack spacing={2} pb={2}>
                  <Descricao>Quanto custa a entrega/frete?</Descricao>
                  <Legenda>
                    Atualmente, estamos fazendo entregas apenas de cestas de café da manhã e de kits
                    de presentes. Cobramos taxa de entrega proporcional ao tempo e distância. É
                    preciso consultar disponibilidade.
                  </Legenda>
                </Stack>

                <Stack spacing={2} pb={2}>
                  <Descricao>Como funcionam as reservas/encomendas?</Descricao>
                  <Legenda>
                    Para efetuar qualquer reserva, é necessário o pagamento do{' '}
                    <strong>sinal de 50%</strong>, de modo a resguardar a produção.
                  </Legenda>
                </Stack>

                <Stack spacing={2} pb={2}>
                  <Descricao>Quais as formas de pagamento?</Descricao>
                  <Legenda>
                    Dinheiro, PIX, PicPay (<strong>3%</strong> sobre o valor) e cartão de
                    crédito/débito (<strong>5%</strong> sobre o valor).
                    <br />
                    <br />
                    Nossos dados para pagamento via PIX:
                    <br />
                    Chave: <strong>41.360.598/0001-07</strong>
                    <br />
                    Beneficiário: <strong>Mel Doce Mel</strong>
                    <br />
                    Banco: <strong>Nubank</strong>
                  </Legenda>
                </Stack>
              </Masonry>
            </Box>
          </Stack>
        </Grid>
      </Grid>

      <Footer
        padding={SPACING}
        hostname="meldocemel.com.br"
        por="Letícia Viégas &amp; Hallison Boaventura"
      />
    </>
  );
};

export default Index;
