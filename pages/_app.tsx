import type { AppProps } from 'next/app';
import Head from 'next/head';
import { CssBaseline, ThemeProvider } from '@mui/material';
import { DefaultSeo } from 'next-seo';
import createTheme from 'tema';

const tema = createTheme('light');

const MyApp = ({ Component, pageProps }: AppProps) => (
  <>
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
    </Head>
    <DefaultSeo
      titleTemplate="%s | Mel Doce Mel Chocolateria"
      description="Pães de mel e outros doces artesanais com ingredientes especiais. Encomendas e delivery em Brasília/DF a combinar. Veja nosso catálogo. Todas as imagens são reais."
      themeColor={tema.palette.primary.main}
    />
    <ThemeProvider theme={tema}>
      <CssBaseline />
      <Component {...pageProps} />
    </ThemeProvider>
  </>
);

export default MyApp;
