import { Html, Head, Main, NextScript } from 'next/document';
import Script from 'next/script';

const Document = () => (
  <Html lang="pt-BR">
    <Head>
      <meta charSet="utf-8" />
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="" />
      <link
        rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Montserrat&amp;family=Forum&amp;display=swap"
      />
      <link rel="icon" type="image/png" href="/favicon.png" />

      <Script
        strategy="afterInteractive"
        src="https://www.googletagmanager.com/gtag/js?id=G-MDGNCJWN8C"
      ></Script>
      <Script strategy="afterInteractive" id="google-analytics">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'G-MDGNCJWN8C');
        `}
      </Script>
    </Head>
    <body>
      <Main />
      <NextScript />
    </body>
  </Html>
);

export default Document;
