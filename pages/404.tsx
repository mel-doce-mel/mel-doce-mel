import Error from 'next/error';

const _404 = () => {
  return <Error statusCode={404} title="Esta página não existe" />;
};

export default _404;
