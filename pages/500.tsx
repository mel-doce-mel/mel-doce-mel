import Error from 'next/error';

const _500 = () => {
  return <Error statusCode={500} title="Erro interno" />;
};

export default _500;
