import type { FC } from 'react';
import Image from 'next/image';
import { Link } from '@mui/material';
import whatsAppImg from 'images/whatsapp.svg';

const BotaoWhatsApp: FC<{ phone: string; text?: string }> = ({ phone, text = '' }) => (
  <Link
    href={'https://api.whatsapp.com/send?phone='.concat(phone, '&text=', text)}
    target="_blank"
    style={{ position: 'fixed', bottom: 20, right: 30 }}
  >
    <Image src={whatsAppImg} alt="Fale conosco pelo WhatsApp" width={70} height={70} />
  </Link>
);

export default BotaoWhatsApp;
