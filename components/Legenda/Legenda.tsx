import type { FC } from 'react';
import { Typography, TypographyProps } from '@mui/material';

const Legenda: FC<TypographyProps> = (props) => <Typography {...props} variant="legenda" />;

export default Legenda;
