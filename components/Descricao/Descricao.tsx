import type { FC } from 'react';
import { Typography, TypographyProps } from '@mui/material';

const Descricao: FC<TypographyProps> = (props) => <Typography {...props} variant="descricao" />;

export default Descricao;
