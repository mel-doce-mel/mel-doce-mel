import type { FC } from 'react';
import { Box } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2';
import Image, { ImageProps } from 'next/image';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

export type ImagensGridProps = {
  imgs: {
    src: ImageProps['src'];
    alt: ImageProps['alt'];
  }[];
};

const ImagensGrid: FC<ImagensGridProps> = ({ imgs }) => (
  <>
    {imgs.map(({ src, alt }, index) => (
      <Grid key={index} md={4} display={{ xs: 'none', md: 'block' }} lineHeight={0}>
        <Image src={src} alt={alt} style={{ maxWidth: '100%', height: 'auto' }} />
      </Grid>
    ))}

    <Grid xs={12} display={{ sm: 'none' }} lineHeight={0}>
      <Carousel
        showStatus={false}
        showArrows={false}
        showThumbs={false}
        autoPlay={true}
        infiniteLoop={true}
        renderIndicator={(onClickHandler, isSelected, index, label) => (
          <Box
            component="li"
            sx={{
              mx: 1,
              display: 'inline-block',
              width: (theme) => theme.spacing(1),
              height: (theme) => theme.spacing(1),
              ...{ backgroundColor: isSelected ? 'secondary.main' : '#fff' },
              borderRadius: '50%',
            }}
            value={index}
            onClick={onClickHandler}
          />
        )}
      >
        {imgs.map(({ src, alt }, index) => (
          <Image key={index} src={src} alt={alt} style={{ maxWidth: '100%', height: 'auto' }} />
        ))}
      </Carousel>
    </Grid>
  </>
);

export default ImagensGrid;
