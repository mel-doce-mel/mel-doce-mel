import type { FC } from 'react';
import { Typography, TypographyProps } from '@mui/material';

const Titulo: FC<TypographyProps> = (props) => <Typography {...props} variant="titulo" />;

export default Titulo;
