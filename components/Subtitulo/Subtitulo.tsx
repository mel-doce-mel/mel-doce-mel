import type { FC } from 'react';
import { Typography, TypographyProps } from '@mui/material';

const Subtitulo: FC<TypographyProps> = (props) => <Typography {...props} variant="subtitulo" />;

export default Subtitulo;
