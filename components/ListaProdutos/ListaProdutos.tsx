import type { FC, ReactNode } from 'react';
import { Stack } from '@mui/material';
import { Titulo, Descricao, Legenda, Preco } from '..';

export type ListaProdutosProps = {
  titulo: string | ReactNode;
  itens: {
    id: string;
    descricao: string;
    legenda?: string;
    subitens?: string[];
    valor: number;
  }[];
};

const ListaProdutos: FC<ListaProdutosProps> = ({ titulo, itens }) => (
  <Stack spacing={2}>
    <Titulo>{titulo}</Titulo>
    <Stack spacing={2}>
      {itens.map(({ id, descricao, legenda, subitens, valor }) => (
        <Stack key={id} spacing={2} direction="row" justifyContent="space-between">
          <Stack spacing={1}>
            <Descricao>{descricao}</Descricao>
            {!!legenda && <Legenda>{legenda}</Legenda>}

            {!!subitens && (
              <Stack spacing={2}>
                {subitens.map((subitem) => (
                  <Legenda key={subitem}>{subitem}</Legenda>
                ))}
              </Stack>
            )}
          </Stack>

          <Preco preco={valor} />
        </Stack>
      ))}
    </Stack>
  </Stack>
);

export default ListaProdutos;
