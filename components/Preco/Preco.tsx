import type { FC } from 'react';
import { Typography, TypographyProps } from '@mui/material';
import formatarPreco from 'lib/formatar-preco';

export type PrecoProps = {
  preco: number;
} & TypographyProps;

const Preco: FC<PrecoProps> = ({ children, preco, ...other }) => (
  <Typography {...other} variant="preco" flexShrink={0}>
    {formatarPreco(preco)}
  </Typography>
);

export default Preco;
