import type { FC } from 'react';
import { Box } from '@mui/material';
import { Favorite as HeartIcon } from '@mui/icons-material';
import { Legenda } from '..';

const Descricao: FC<{ padding: number; hostname: string; por: string }> = ({
  padding,
  hostname,
  por,
}) => (
  <Box p={padding} textAlign="center">
    <Legenda>
      © {new Date().getFullYear()} {hostname} — Feito com
    </Legenda>
    <HeartIcon color="error" fontSize="small" sx={{ mx: 0.5, verticalAlign: 'middle' }} />
    <Legenda>por {por}</Legenda>
  </Box>
);

export default Descricao;
