import { CSSProperties } from 'react';
import { PaletteMode } from '@mui/material';
import { createTheme, responsiveFontSizes, Palette, PaletteOptions } from '@mui/material/styles';

// https://mui.com/material-ui/customization/typography/#adding-amp-disabling-variants
declare module '@mui/material/styles' {
  interface TypographyVariants {
    titulo: CSSProperties;
    subtitulo: CSSProperties;
    descricao: CSSProperties;
    legenda: CSSProperties;
    preco: CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    titulo?: CSSProperties;
    subtitulo?: CSSProperties;
    descricao?: CSSProperties;
    legenda?: CSSProperties;
    preco?: CSSProperties;
  }

  interface Palette {
    tertiary: Palette['primary'];
  }

  interface PaletteOptions {
    tertiary: PaletteOptions['primary'];
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    // Enabling
    titulo: true;
    subtitulo: true;
    descricao: true;
    legenda: true;
    preco: true;

    // Disabling
    h1: false;
    h2: false;
    h3: false;
    h4: false;
    h5: false;
    h6: false;
    subtitle1: false;
    subtitle2: false;
    body1: false;
    body2: false;
    button: false;
    caption: false;
    overline: false;
  }
}

export default (mode: PaletteMode) => {
  const tema = createTheme({
    palette: {
      primary: { main: '#fefbf0' },
      secondary: { main: '#7ac1c8' },
      tertiary: { main: '#edf6f8' },
      text: { primary: '#794e1d' },
    },
    typography: {
      titulo: {
        fontFamily: 'Forum, serif',
        fontWeight: 400,
        fontSize: '2rem',
        lineHeight: 1,
      },
      subtitulo: {
        fontFamily: 'Forum, serif',
        fontSize: '1.5rem',
        lineHeight: 1,
      },
      descricao: {
        fontFamily: 'Montserrat, sans-serif',
        fontSize: '1.3rem',
        lineHeight: 1.5,
      },
      legenda: {
        fontFamily: 'Montserrat, sans-serif',
        fontSize: '0.875rem',
        lineHeight: 1.5,
      },
      preco: {
        fontFamily: 'Montserrat, sans-serif',
        fontSize: '1.3rem',
        lineHeight: 1.5,
      },
      button: {
        fontFamily: 'Montserrat, serif',
        fontWeight: 400,
      },
    },
    components: {
      MuiTypography: {
        defaultProps: {
          variantMapping: {
            titulo: 'h2',
            subtitulo: 'h3',
            descricao: 'p',
            legenda: 'small',
            preco: 'p',
          },
        },
      },
      MuiAppBar: {
        styleOverrides: {
          colorPrimary: ({ theme }) => ({
            backgroundColor: theme.palette.tertiary.main,
          }),
        },
      },
    },
  });

  return responsiveFontSizes(tema);
};
