const formatarPreco = (n: number) => {
  const x = n / 100;

  if (!Number.isInteger(x)) return 'R$ '.concat(x.toFixed(2));

  return 'R$ '.concat(x.toString());
};

export default formatarPreco;
