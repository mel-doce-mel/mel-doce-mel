export enum Link {
  Instagram ='https://www.instagram.com/meldocemel.chocolateria/',
  WhatsApp = 'https://wa.me/5561996657197?text=',
  GoogleMaps = 'https://goo.gl/maps/Ub5Y41WroQzKDoBP6',
};

export enum Tamanho {
  Mini = 'Mini',
  Pequeno = 'Pequeno',
  Medio = 'Médio',
  Grande = 'Grande',
}

export enum TipoPaoDeMel {
  Tradicional = 'Tradicional',
  Trufado = 'Trufado',
}

export type PaoDeMel = {
  id: string;
  tipo: TipoPaoDeMel;
  tamanho: Tamanho;
  diametro: number;
  valor: number;
};

export const paesDeMel: PaoDeMel[] = [
  {
    id: 'aa303a21-fee2-49db-beff-a7cec53d11f1',
    tipo: TipoPaoDeMel.Tradicional,
    tamanho: Tamanho.Mini,
    diametro: 4,
    valor: 550,
  },
  {
    id: '7907fa64-1a74-4cbb-9108-5c7bb9782183',
    tipo: TipoPaoDeMel.Tradicional,
    tamanho: Tamanho.Medio,
    diametro: 5,
    valor: 650,
  },
  {
    id: '5db93faf-2552-4fa4-b42c-6b77d5bdfa99',
    tipo: TipoPaoDeMel.Tradicional,
    tamanho: Tamanho.Grande,
    diametro: 6,
    valor: 850,
  },
  {
    id: 'd1687458-3de2-4458-a652-f43bcb15470d',
    tipo: TipoPaoDeMel.Trufado,
    tamanho: Tamanho.Mini,
    diametro: 4,
    valor: 600,
  },
  {
    id: 'de7e1884-46f6-4a6c-95f0-94cebf4153d6',
    tipo: TipoPaoDeMel.Trufado,
    tamanho: Tamanho.Medio,
    diametro: 5,
    valor: 700,
  },
  {
    id: '88e1e685-3c12-4ed4-a050-ca474b905d46',
    tipo: TipoPaoDeMel.Trufado,
    tamanho: Tamanho.Grande,
    diametro: 6,
    valor: 900,
  },
];

export enum Material {
  Cetim = 'Cetim',
  Cordone = 'Cordone',
  Gorgurao = 'Gorgurão',
  Papel = 'Papel',
}

export type OrnamentacaoPaoDeMel = {
  id: string;
  descricao: string;
  material: Material;
  valor: number;
};

export const ornamentacoesPaesDeMel: OrnamentacaoPaoDeMel[] = [
  {
    id: '4756d42e-7523-4b7e-a34f-c8c737f5e8a5',
    descricao: 'Laço simples',
    material: Material.Cetim,
    valor: 70,
  },
  {
    id: '8ec8581b-79d4-4e0c-8947-cebd7baf59db',
    descricao: 'Laço metalizado',
    material: Material.Cordone,
    valor: 70,
  },
  {
    id: '2fefe12d-43f2-4cd3-81c1-617e48ea7a4c',
    descricao: 'Laço chanel',
    material: Material.Cetim,
    valor: 70,
  },
  // {
  //   id: '5c105b59-792c-48ce-9c6f-30a1432ef569',
  //   descricao: 'Laço chanel',
  //   material: Material.Gorgurao,
  //   valor: 60,
  // },
  {
    id: '417e4d08-aa13-43b7-ab9c-aa138b76206b',
    descricao: 'Arte para tag',
    material: Material.Papel,
    valor: 6000,
  },
];

export enum SaborAlfajor {
  DoceDeLeite = 'Doce de leite',
  Limao = 'Limão',
}

export type Alfajor = {
  id: string;
  sabor: SaborAlfajor;
  diametro: number;
  tamanho: Tamanho;
  valor: number;
};

export const alfajores: Alfajor[] = [
  {
    id: 'd3bbe3f3-ec6a-4030-836b-7b3fa2ef4dbc',
    sabor: SaborAlfajor.DoceDeLeite,
    diametro: 5,
    tamanho: Tamanho.Medio,
    valor: 700,
  },
  /*{
    id: 'b75898f8-59d7-4fd0-827a-32d6e43df4e9',
    sabor: SaborAlfajor.Limao,
    diametro: 5,
    tamanho: Tamanho.Medio,
    valor: 700,
  },*/
];

export type Brownie = {
  id: string;
  tamanho: Tamanho;
  diametro: number;
  valor: number;
};

export const brownies: Brownie[] = [
  {
    id: '312d8fc9-0027-4eeb-91cb-2fd5b8120af2',
    tamanho: Tamanho.Mini,
    diametro: 4,
    valor: 550,
  },
  {
    id: 'f3270e30-f063-41fb-a5c8-51a6fabce748',
    tamanho: Tamanho.Medio,
    diametro: 5,
    valor: 750,
  },
  {
    id: '64bb9f73-cccd-4de9-8b53-ff3027664dc6',
    tamanho: Tamanho.Grande,
    diametro: 6,
    valor: 850,
  },
];

export enum TipoCookie {
  Tradicional = 'Tradicional',
  Fudge = 'Fudge',
}

export type Cookie = {
  id: string;
  tipo: TipoCookie;
  tamanho: Tamanho;
  diametro: number;
  valor: number;
};

export const cookies: Cookie[] = [
  {
    id: 'e3a551c9-c05b-4501-8c70-8bcdad346f30',
    tipo: TipoCookie.Tradicional,
    tamanho: Tamanho.Grande,
    diametro: 8,
    valor: 550,
  },
  {
    id: '76ab6b99-0e59-42bd-80dd-e4792fc8c2d3',
    tipo: TipoCookie.Fudge,
    tamanho: Tamanho.Grande,
    diametro: 8,
    valor: 600,
  },
];

export enum SaborBarraRecheada {
  CarameloSalgado = 'Caramelo salgado',
  BrigadeiroDeCafe = 'Brigadeiro de café',
  Oreo = 'Oreo ™',
}

export type BarraRecheada = {
  id: string;
  sabor: SaborBarraRecheada;
  comprimento: number;
  valor: number;
};

export const barrasRecheadas: BarraRecheada[] = [
  {
    id: '30176923-4b7c-492c-bb3e-14bff88555f3',
    sabor: SaborBarraRecheada.CarameloSalgado,
    comprimento: 8,
    valor: 800,
  },
  /* 
  {
    id: '0c892c78-3cc2-4cbe-a944-cdc36bcb099e',
    sabor: SaborBarraRecheada.BrigadeiroDeCafe,
    comprimento: 8,
    valor: 750,
  },
  */
  {
    id: 'ef22fce0-3e1f-44f9-937c-0bd0ff11d2f1',
    sabor: SaborBarraRecheada.Oreo,
    comprimento: 8,
    valor: 800,
  },
];

export type PalhaItaliana = {
  id: string;
  tamanho: Tamanho;
  lado: number;
  valor: number;
};

export const palhasItalianas: PalhaItaliana[] = [
  {
    id: '660e7bea-e48f-4ed5-a307-e36d21b14990',
    tamanho: Tamanho.Mini,
    lado: 3,
    valor: 550,
  },
  {
    id: 'c661c708-815b-4d83-9c19-e14dce25733f',
    tamanho: Tamanho.Medio,
    lado: 4,
    valor: 700,
  },
  {
    id: '09b092cb-05a1-4b3c-aef8-c709744d5813',
    tamanho: Tamanho.Grande,
    lado: 5,
    valor: 850,
  },
];

export type CremeDeCafe = {
  id: string;
  peso: number;
  valor: number;
};
/*
export const cremesDeCafe: CremeDeCafe[] = [
  { id: 'dff67ec9-e8ce-4f5a-a97e-cd06666636c6', peso: 80, valor: 950 },
];
*/

export enum SaborBiscoitoAmanteigado {
  NataComGranulado = 'Nata com granulado de chocolate',
  Parmesao = 'Parmesão',
  Nata = 'Nata',
  Amendoas = 'Amêndoas',
}

export type BiscoitoAmanteigado = {
  id: string;
  sabor: SaborBiscoitoAmanteigado;
  peso: number;
  valor: number;
};

export const biscoitosAmanteigados: BiscoitoAmanteigado[] = [
  {
    id: '2042e0d1-332f-47a0-890b-b99efa47a80a',
    sabor: SaborBiscoitoAmanteigado.NataComGranulado,
    peso: 100,
    valor: 890,
  },
  {
    id: '1ef1bdfa-426a-4d03-89cd-422808014ff6',
    sabor: SaborBiscoitoAmanteigado.Parmesao,
    peso: 100,
    valor: 890,
  },
  {
    id: '8289c194-f603-457e-9305-0f5a53111c93',
    sabor: SaborBiscoitoAmanteigado.Nata,
    peso: 100,
    valor: 890,
  },
  {
    id: '1abd6c22-4bc2-46f1-ae23-708ac253fba2',
    sabor: SaborBiscoitoAmanteigado.Amendoas,
    peso: 100,
    valor: 1500,
  },
];

export type EmbalagemParaPresente = {
  id: string;
  descricao: string;
  itens: string;
  valor: number;
};

export const embalagensParaPresente: EmbalagemParaPresente[] = [
  {
    id: '7bf7ef30-efc4-4e79-8120-9e3c99fa17bf',
    descricao: 'Caixa Mel Doce Mel',
    itens: 'Caixa de papel, fita de cetim, saco de celofane e tag',
    valor: 1200,
  },
];

export type ItemDaCestaDeCafeDaManha = string;

const itensDaCestaDeCafeDaManha: ItemDaCestaDeCafeDaManha[] = [
  'Alfajores de doce de leite médios',
  'Biscoito amanteigado nata',
  'Biscoito amanteigado maizena',
  'Biscoito amanteigado limão e canela',
  'Brownies',
  'Cappuccino',
  'Cookie',
  'Frutas do dia',
  'Geleia de fruta',
  'Iogurte',
  'Manteiga',
  'Mini bolo caseiro',
  'Mussarela',
  'Peito de peru',
  'Pães de mel tradicionais médio',
  'Pão de queijo',
  'Pão francês',
  'Suco de fruta',
];

export enum PorcaoCestaDeCafeDaManha {
  Individual = 'Servem duas pessoas',
}

export type CestaDeCafeDaManha = {
  id: string;
  porcao: PorcaoCestaDeCafeDaManha;
  itens: ItemDaCestaDeCafeDaManha[];
  valor: number;
};

export const cestasDeCafeDaManha: CestaDeCafeDaManha[] = [
  {
    id: '85ecdb51-e26e-45bd-9a8d-fffeb48a5dc1',
    porcao: PorcaoCestaDeCafeDaManha.Individual,
    itens: itensDaCestaDeCafeDaManha,
    valor: 25000,
  },
  /*{
    id: 'db47963c-fb60-4da0-b04e-281bfaaae4c2',
    porcao: PorcaoCestaDeCafeDaManha.Dupla,
    itens: itensDaCestaDeCafeDaManha,
    valor: 31000,
  },*/
];

export type ItemKit = string;

const itensKitPaoeAlfajor: ItemKit[] = [
  '2x pães de mel tradicionais médio',
  '2x alfajores de doce de leite médio',
];

const itensKitPao: ItemKit[] = [
  '5x pães de mel tradicionais médio',
];

const itensKitBiscoitos: ItemKit[] = [
  '1x Biscoito amanteigado de Nata com granulado de chocolate 150g',
  '1x Biscoito amanteigado de Nata 150g',
];

const itensKitPaoeBiscoito: ItemKit[] = [
  '3x pães de mel tradicionais médio',
  '1x Bicoito amanteigado 100g',
];


export type Kit = {
  id: string;
  tamanho: Tamanho;
  titulo: string;
  itens: ItemKit[];
  valor: number;
};

export const kits: Kit[] = [
  {
    id: '588c479e-2da6-4aa4-b74b-4fdc5ec6ecc0',
    tamanho: Tamanho.Pequeno,
    titulo: 'Pão de mel e Alfajor',
    itens: itensKitPaoeAlfajor,
    valor: 3900,
  },
  {
    id: '07a2a75f-3c8b-486e-8892-c018c5d9004d',
    tamanho: Tamanho.Grande,
    titulo: 'Pão de mel',
    itens: itensKitPao,
    valor: 4500,
  },
  {
    id: '07a2a75f-3c8b-486e-8892-c018c5d9004e',
    tamanho: Tamanho.Grande,
    titulo: 'Biscoitos amanteigados',
    itens: itensKitBiscoitos,
    valor: 3900,
  },
  {
    id: '07a2a75f-3c8b-486e-8892-c018c5d9004f',
    tamanho: Tamanho.Grande,
    titulo: 'Pão de mel e Biscoito amanteigado',
    itens: itensKitPaoeBiscoito,
    valor: 4000,
  },
];
